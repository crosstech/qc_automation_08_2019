<?php
namespace Page\Acceptance;

class LoginApi
{
	// include url of current page
	public static $URL = '/api/user/login';

	public static $URLAllUser = '/api/admin/user/all';

	public static $userName = 'username';

	public static $password = 'password';
}
