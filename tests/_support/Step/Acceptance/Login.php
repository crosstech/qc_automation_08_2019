<?php
namespace Step\Acceptance;
use Page\Acceptance\Login as LoginPage;
class Login extends \AcceptanceTester
{
	/**
	 * @param $user
	 * @param $password
	 * @throws \Exception
	 */
	public function checking($user, $password)
	{
		$client = $this;
		$client->amOnPage(LoginPage::$URL);
		$client->waitForElement(LoginPage::$username, 30);
		$client->waitForElementVisible(LoginPage::$username, 30);
		$client->fillField(LoginPage::$username, $user);
		$client->fillField(LoginPage::$password, $password);
		$client->click(LoginPage::$login);
		$client->see(LoginPage::$controlPage);
	}
}