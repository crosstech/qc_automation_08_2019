<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
use Page\Acceptance\Login as LoginPage;
class AcceptanceTester extends \Codeception\Actor
{
	use _generated\AcceptanceTesterActions;

   /**
	* Define custom actions here
	*/

	/**
	 * @param $user
	 * @param $password
	 * @throws \Exception
	 */
	public function login($user, $password)
	{
		$client = $this;
		$client->amOnPage(LoginPage::$URL);
		$client->waitForElement(LoginPage::$username, 30);
		$client->waitForElementVisible(LoginPage::$username, 30);
		$client->fillField(LoginPage::$username, $user);
		$client->fillField(LoginPage::$password, $password);
		$client->click(LoginPage::$login);
		$client->see(LoginPage::$controlPage);
	}
}
