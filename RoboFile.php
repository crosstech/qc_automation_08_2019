<?php
/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */

class RoboFile extends \Robo\Tasks
{
	use Joomla\Testing\Robo\Tasks\LoadTasks;
	function runAcceptance($debug = true, $steps = true)
	{
		$args = [];

		if ($debug)
		{
			$args[] = '--debug';
		}

		if ($steps)
		{
			$args[] = '--steps';
		}
		$this->taskCodecept()
			->args($args)
			->arg('tests/acceptance/')
			->run()
			->stopOnFail();
	}

	public function runApi()
	{
		$this->taskCodecept()
			->debug()
			->arg('tests/api/')
			->run()
			->stopOnFail();
	}
}